import youtubeVideo from './components/youtube_video'

youtubeVideo()

$('.responsive').slick({
  dots: true,
  prevArrow: $('.prev'),
  nextArrow: $('.next'),
  infinite: true,
  speed: 200,
  slidesToShow: 6,
  slidesToScroll: 7,
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
        infinite: true,
        dots: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),

      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),

      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
