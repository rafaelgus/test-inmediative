<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<meta name="MobileOptimized" content="320">
<meta name="theme-color" content="#E44D26">
<meta name="msapplication-TileColor" content="#E44D26">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="default"/>
<link rel="author" type="text/plain" href="./humans.txt"/>
<link rel="sitemap" type="application/xml" title="Sitemap" href="./sitemap.xml"/>
<title>Rherrera Test</title>
<meta name="description" content="Kit  de inicio preparado para desarrollo frontend (HTML, Sass y JS) y/o backend (PHP) basado en componentes y automatizado con Gulp. esto para test en Inmediative">
<meta name="application-name" content="Gulp Starter Kit">
