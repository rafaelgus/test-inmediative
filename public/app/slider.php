<div class="container">
  <div class="row">
    <div class="col-md-12 heroSlider-fixed">
      <div class="overlay"></div>
        <div><h1>Screenshots</h1></div>
          <div class="slider responsive item__padding">
              <?php foreach ($data as $data->slide => $value) : ?>
                <div class="carousel-item active">
                    <img class="img-fluid"src="<?php echo $slide ?>" alt=" slide">
                </div>
              <?php endforeach; ?>
              <div></div>
            </div>
                <!-- control arrows -->
            <div class="prev">
              <span ><i class="fas fa-angle-left fa-2x"></i></span>
            </div>
            <div class="next">
            <span ><i class="fas fa-angle-right fa-2x"></i></span>
          </div>
        </div>
    </div>
  </div>
</div>
