<!DOCTYPE html>
<html lang="es">
  <head>
    <?php require './app/meta_tags.php'; ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.css'>
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
   <?php require './app/db_json.php'; ?>
      <main class="app" >
        <header class="header">
          <h1><?php echo $title ?></h1>
        </header>
        <nav>
          <?php require './app/nav.php'; ?>
        </nav>
        <section class="jumbotron jumbotron-item"><?php require './app/jumbotron.php'; ?></section>
        <article class="article">
          <h1><?php echo $box ?></h1>
          <p><?php echo $boxtext ?></p>
        </article>
        <aside>
          <h1>Moment</h1>
          <?php foreach ($data as $data->moment => $value) : ?>
          <li><?php echo $moment ?></li>
          <?php endforeach; ?>
        </aside>
        <div class="especial"></div>
        <!-- <div class="especial"></div> -->
        <section class="slider"><?php require './app/slider.php'; ?></section>
        <footer>
          <p class="text__footer">Maquetado por Rafael Herrera | 2018</p>
        </footer>



      </main>



    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.js'></script>
    <script src="./js/script.js"></script>


  </body>
</html>

