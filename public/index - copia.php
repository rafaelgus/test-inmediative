<!DOCTYPE html>
<html lang="es">
  <head>
    <?php require './app/meta_tags.php'; ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.css'>
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
   <?php require './app/db_json.php'; ?>
      <main class="container" >




        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-xl-6 text-justify ">
                  <h1><?php echo $box ?></h1>
                  <p><?php echo $boxtext ?></p>
                </div>
                <div class="col-xs-12 col-xl-6  text-center">
                  <h1>Moment</h1>
                    <?php foreach ($data as $data->moment => $value) : ?>
                    <li><?php echo $moment ?></li>
                    <?php endforeach; ?>
              </div>
            </div>
        </div>


      <footer class="footer text-muted">
          <div class="container">
            <p class="text__footer">Maquetado por Rafael Herrera | 2018</p>
          </div>
      </footer>
    </main>






    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.js'></script>
    <script src="./js/script.js"></script>


  </body>
</html>

